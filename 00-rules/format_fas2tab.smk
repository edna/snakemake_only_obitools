### Some unuseful attributes can be removed at this stage
rule rm_attributes:
    input:
        '03-assigned/{run}_run.tag.u.fasta'
    output:
        '04-formated/{run}_run.a.t.u.fasta'
    singularity:
        config["container"]
    conda:
        '../96-envs/obitools_env.yaml'
    log:
        '../99-log/11-rm_attributes/{run}.log'
    shell:
        '''obiannotate --delete-tag=scientific_name_by_db --delete-tag=obiclean_samplecount \
 --delete-tag=obiclean_count --delete-tag=obiclean_singletoncount \
 --delete-tag=obiclean_cluster --delete-tag=obiclean_internalcount \
 --delete-tag=obiclean_head  --delete-tag=obiclean_headcount \
 --delete-tag=id_status --delete-tag=rank_by_db --delete-tag=obiclean_status \
 --delete-tag=seq_length_ori --delete-tag=sminL --delete-tag=sminR \
 --delete-tag=reverse_score --delete-tag=reverse_primer --delete-tag=reverse_match --delete-tag=reverse_tag \
 --delete-tag=forward_tag --delete-tag=forward_score --delete-tag=forward_primer --delete-tag=forward_match \
 --delete-tag=tail_quality {input} > {output} 2> {log}'''

### The sequences can be sorted by decreasing order of count
rule sort_runs:
    input:
        '04-formated/{run}_run.a.t.u.fasta'
    output:
        '04-formated/{run}_run.s.a.t.u.fasta'
    singularity:
        config["container"]
    conda:
        '../96-envs/obitools_env.yaml'
    log:
        '../99-log/12-sort_runs/{run}.log'
    shell:
        '''obisort -k count -r {input} > {output} 2> {log}'''

### Generate a table final results
rule table_runs:
    input:
        '04-formated/{run}_run.s.a.t.u.fasta'
    output:
        '../04-final_tables/{run}.csv'
    singularity:
        config["container"]
    conda:
        '../96-envs/obitools_env.yaml'
    log:
        '../99-log/13-table_runs/{run}.log'
    shell:
        '''obitab -o {input} > {output} 2> {log}'''
