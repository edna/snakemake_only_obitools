### Dereplicate and merge samples together
rule dereplicate_runs:
    input:
        '01-runs/{run}_run.fasta'
    output:
         '02-dereplicated/{run}_run.uniq.fasta'
    singularity:
        config["container"]
    conda:
        '../96-envs/obitools_env.yaml'
    log:
         '../99-log/09-dereplicate_runs/{run}.log'
    shell:
         '''obiuniq -m sample {input} > {output} 2> {log}'''
