### Assign each sequence record to the corresponding sample/marker combination
rule assign_sequences:
    input:
        '02-remove_unaligned/{run}.ali.fastq',
    output:
        assign='03-assign_sequences/{run}.ali.assigned.fastq',
        unid='03-assign_sequences/{run}.unidentified.fastq'
    singularity:
        config["container"]
    conda:
        '../96-envs/obitools_env.yaml'
    params:
        barcodeFile=lambda wcs: config["barcodeFiles"][wcs.run],
        barcodeFolder=config['fastqFolderPath']
    log:
        '../99-log/01-assembly/03-assign_sequences/{run}.log'
    shell:
        '''ngsfilter -t {params.barcodeFolder}{params.barcodeFile} -u {output.unid} {input} --fasta-output > {output.assign} 2> {log}'''


### Split the input sequence file in a set of subfiles according to the values of attribute `sample`
rule split_sequences:
    input:
        '03-assign_sequences/{run}.ali.assigned.fastq'
    params:
        dir='../02-demultiplex/01-raw/{run}/'
    singularity:
        config["container"]
    conda:
        '../96-envs/obitools_env.yaml'
    log:
        '../99-log/01-assembly/04-split_sequences/{run}.log'
    shell:
        '''mkdir -p {params.dir}; obisplit -p "{params}" -t sample --fasta {input} 2> {log}'''
