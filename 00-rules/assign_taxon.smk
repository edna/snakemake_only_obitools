### Assign each sequence to a taxon
rule assign_taxon:
    input:
        '02-dereplicated/{run}_run.uniq.fasta'
    output:
        '03-assigned/{run}_run.tag.u.fasta'
    singularity:
        config["container"]
    conda:
        '../96-envs/obitools_env.yaml'
    params:
        bdr=config["assign_taxon"]["bdr"],
        fasta=config["assign_taxon"]["fasta"]
    log:
        '../99-log/10-assign_taxon/{run}.log'
    shell:
        '''ecotag -d {params.bdr} -R {params.fasta} {input} > {output} 2> {log}'''
