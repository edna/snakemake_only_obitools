__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


### Paired end alignment then keep reads with quality > 40
rule illuminapairedend:
    input:
        R1=config['fastqFolderPath']+'{run}_R1.fastq.gz',
        R2=config['fastqFolderPath']+'{run}_R2.fastq.gz'
    output:
        fq='01-illuminapairedend/{run}.fastq'
    singularity:
        config["container"]
    conda:
        '../96-envs/obitools_env.yaml'
    log:
        '../99-log/01-assembly/01-illuminapairedend/{run}.log'
    params:
        s_min=config["illuminapairedend"]["s_min"]
    shell:
        '''illuminapairedend -r {input.R2} {input.R1} --score-min={params.s_min} > {output.fq} 2> {log}'''

### Remove unaligned sequence records
rule remove_unaligned:
    input:
        fq='01-illuminapairedend/{run}.fastq'
    output:
        ali='02-remove_unaligned/{run}.ali.fastq'
    singularity:
        config["container"]
    conda:
        '../96-envs/obitools_env.yaml'
    log:
        '../99-log/01-assembly/02-remove_unaligned/{run}.log'
    shell:
        '''obigrep -p 'mode!=\"joined\"' {input.fq} > {output.ali} 2> {log}'''

