### dereplicate reads into uniq sequences
rule dereplicate_samples:
    input:
        'samples/{sample}.fasta'
    output:
        'samples/{sample}.uniq.fasta'
    log:
        'log/dereplicate_samples/{sample}.log'
    shell:
        '''obiuniq -m sample {input} > {output} 2> {log}'''

### only sequence more than 20bp with no ambiguity IUAPC with total coverage greater than 10 reads
rule goodlength_samples:
    input:
        'samples/{sample}.uniq.fasta'
    output:
        'samples/{sample}.l.u.fasta'
    log:
        'log/goodlength_samples/{sample}.log'
    params:
         count=config["good_length_samples"]["count"],
         seq_length=config["good_length_samples"]["seq_length"]
    shell:
        '''obigrep  -p 'count>{params.count}' -s '^[ACGT]+$' -p 'seq_length>{params.seq_length}' {input} > {output} 2> {log}'''

### Clean the sequences for PCR/sequencing errors (sequence variants)
rule clean_pcrerr_samples:
    input:
        'samples/{sample}.l.u.fasta'
    output:
        'samples/{sample}.r.l.u.fasta'
    log:
        'log/clean_pcrerr/{sample}.log'
    params:
         r=config["clean_pcrerr_samples"]["r"]
    shell:
        '''obiclean -r {params.r} {input} > {output} 2> {log}'''

### Remove sequence which are classified as 'internal' by obiclean
rule rm_internal_samples:
    input:
        'samples/{sample}.r.l.u.fasta'
    output:
        'samples/{sample}.c.r.l.u.fasta'
    log:
        'log/rm_internal_samples/{sample}.log'
    shell:
        '''obigrep -p 'obiclean_internalcount == 0' {input} > {output} 2> {log}'''
