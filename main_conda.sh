
###############################################################################
## Here, we reproduce the bioinformatics workflow used by SPYGEN 
## We improve its performance by adding parallelization and containerazition features.
##
## This workflow generates species environmental presence from raw eDNA data. 
##
## This workflow use the workflow management system SNAKEMAKE.
## 
##
## Author : GUERIN Pierre-Edouard
## Montpellier 2019-2020
## 
###############################################################################
## Usage:
##    CORE=16
##    CONFIG_FILE="config.yaml"
##    bash main.sh $CORES $CONFIG_FILE
##
##
###############################################################################
CORES=$1
CONFIGFILE=$2

#CORES=16
#CONFIGFILE="config.yaml"

###############################################################################
## assemble & demultiplex
cd 01-assembly
snakemake -s Snakefile -j $CORES --use-conda --latency-wait 120 --configfile "../"$CONFIGFILE
cd ..
###############################################################################
## filter sequences
cd 02-demultiplex
snakemake -s Snakefile -j $CORES --use-conda --latency-wait 120 --configfile "../"$CONFIGFILE
cd ..
###############################################################################
## concatenate samples into run
for run in `ls 02-demultiplex/03-cleaned/`;
do cat 02-demultiplex/03-cleaned/${run}/*.c.r.l.u.fasta > 03-filtered/01-runs/${run}_run.fasta ;
done
###############################################################################
## taxonomic assignation & format
cd 03-filtered
#snakemake -s Snakefile -j 8 --dry-run --use-singularity --singularity-args "--bind /media/superdisk:/media/superdisk" --latency-wait 120
snakemake -s Snakefile -j $CORES --use-conda --latency-wait 120 --configfile "../"$CONFIGFILE
cd ..

###############################################################################
## clean
#snakemake -s Snakefile --delete-all-output --dry-run
#snakemake -s Snakefile --delete-all-output

